<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
   <body>
    
    <?php include 'static/navbar.php'; ?>
    <!-- Navigation -->

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Potensi Wilayah
        <small>Desa Wukirsari
        </small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Potensi Desa</li>
      </ol>

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

          <!-- Preview Image -->
          <!-- <img class="img-fluid rounded" src="http://placehold.it/900x300" alt=""> -->

          <hr>

          <p>Berikut ini adalah 5 titik potensi wisata yang sedang menjadi fokus untuk dikembangan oleh Desa Wukirsari :</p>

<h2> 1. Dusun Cancangan </h2>

<p style="text-align: justify;">Dusun cancangan merupakan dusun yang terletak di dalam desa wukirsari. Perjalanan yang ditempuh dari kota Yogyakarta untuk sampai ke dusun cancangan yaitu kurang lebih 40 menit. Potensi wisata yang dimiliki oleh dusun cancangan sangatlah beragam. Salah satunya yaitu potensi wisata tentang kawasan studi  dan konservasi burung hantu. Konservasi ini terbentuk dikarenakan keresahan warga dusun cancangan oleh hama seperti tikus dan serangga serangga lainnya yang mengganggu sistem bercocok tanam warga sekitar, karena burung hantu merupakan pemangsa sekaligus penjaga yang baik untuk menjaga sawah warga dari serangan hama. Berawal dari keresahan warga hingga sekarang burung hantu disana sudah dikembang biakan dan menjadi sebuah konservasi dan bahkan menjadi kawasan studi untuk burung hantu. Sudah banyak para peneliti yang tertarik dengan burung hantu mengunjungi dusun cancangan hanya untuk mempelajari dan meneliti langsung tentang burung hantu yang telah menjaga ladang dan sawah warga dusun cancangan.</p>

<h2> 2. Dusun Brayut </h2>

<p style="text-align: justify;">Dusun brayut merupakan salah satu dusun yang terletak di desa wukirsari yang memiliki potensi ekowisata yaitu pohon jati dan sungai . Kenapa dikatakan ekowisata karena dusun brayut memiliki lahan yang ditumbuhi beraneka ragam tumbuh-tumbuhan yang dimanfaatkan untuk wilayah ekowisata. Tumbuhan yang terdapat di dusun brayut mulai dari tumbuhan obat, pepohonan yang rindang, serta perkebunan yang menyediakan berbagai sayur sayuran yang biasa digunakan oleh warga untuk konsumsi sehari-hari bahkan untuk diperdagangkan untuk menambah penghasilan warga. Berbagai macam pengunjung datang ke dusun hanya untuk menikmati alamnya yang rimbun dan menyegarkan. Banyak juga para pemborong datang dari kota untuk membeli hasil perkebunan dusun brayut.</p>

<h2> 3. Dusun Balangan </h2>

<p style="text-align: justify;">Dusun yang satu ini juga tak kalah indahnya dengan dusun lain. Dusun balangan menyediakan potensi wisata rekreasi alam. Dusun balangan memiliki lahan yang sangat luas dan didukung dengan suasana yang menenangkan. Tempatnya sangat cocok untuk rekreasi keluarga karena di dusun balangan menyediakan tempat untuk camping ground dan outbond sehingga keluarga yang datang kesini merasa nyaman menyenangkan setelah merasa lelah dengan kehidupan di kota. Warganya juga yang sangat ramah menambah nilai kepuasan tersendiri dari dusun balangan desa wukirsari ini. Dusun balangan juga memiliki balai pusat pelatihan pertanian. Balai tersebut digunakan untuk para petani untuk berkumpul dan berbagi ilmu serta pelatihan pelatihan tentang bercocok tanam. Sayuran dan buah buahan yang ditanam di dusun balangan adalah sayuran organic yang sangat sehat tanpa pestisida dan sangat bermanfaat bagi kesehatan.</p>

<h2> 4. Dusun Huntap Gondang 2 </h2>

<p style="text-align: justify;">Dusun Gondang adalah sebuah dusun yang tidak hanya ada interaksi antar masyarakat setempat saja. Lebih dari itu seperti Dusun Gondang menyimpan banyak potensi wisata. Salah satunya pemanfaatan sumber daya alam sebagai tempat rekreasi alam yang memiliki nilai jual. Di Dusun Gondang sendiri kita dapat menikmati indahnya alam nan indah, sejuk, dan damai yang terhindar dari hiruk pikuk kota. Di Dusun ini terdapat Camping Ground. Jadi, bagi teman-teman yang suka ngecamp bisa langsung ke Dusun ini. Untuk camping ground nya terdapat lahan outbound yang luas. Kita dengan leluasa menikamti indahnya alam nan asri.  Untuk akses pun kita tidak susah, kita hanya menempuh waktu sekitar 30 menit dari Kota Yogkarta saja.

Potensi wisata Dusun Gondang Sendiri sudah baik dalam pegelolaanya. Hal ini terlihat dari manajemen yang sudah tertata rapih seperti pengelolaanya baik dari pemeliharaan wisata alam nya sendiri maupun akses bagi wisatawan untuk menikmati panorama alam nan indah dan sejuk dari Dusun Gondang itu sendiri. Namun mungkin bisa diperhatikan untuk lebih mengoptimalkan lagi pengelolaan sehingga potensi wisata Dusun Gondang bisa menjadi destinasi wisata unggulan.</p>

<h2> 5. Dusun Bulak Salak </h2>

<p style="text-align: justify;">Sebuah Dusun yang memiliki keunikan tersendiri. Salah satu keunikanya yakni di Dusun ini ditumbuhi begitu banyak keanekaragaman jenis tanaman bambu .Sangat unik, bambu yang dulunya dimanfaatkan sebagai bahan baku pembuatan rumah, sekarang sedikit bergeser sebagai bahan yang memiliki nilai jual tinggi. Bambu di Dusun Bulak Salak mewadai para peneliti untuk meneliti jenis-jenis bambu yang tumbuh di Dusun terebut. Tidak hanya meneliti, wisatawan pun yang merasa penasaran dengan tumbuhan yang berbuku yang begitu banyak tumbuh di Dusun Bulak Salak, dapat dengan mudah  mengunjungi Dusun ini.
</p>
          <hr>

          <!-- Comments Form -->
          <!-- <div class="card my-4">
            <h5 class="card-header">Tinggalkan Komentar :</h5>
            <div class="card-body">
              <form>
                <div class="form-group">
                  <textarea class="form-control" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
              </form>
            </div>
          </div> -->

          <!-- Single Comment -->
          <!-- <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
            <div class="media-body">
              <h5 class="mt-0">Commenter Name</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
            </div>
          </div> -->

          <!-- Comment with nested comments -->
          <!-- <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
            <div class="media-body">
              <h5 class="mt-0">Commenter Name</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.

              <div class="media mt-4">
                <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                <div class="media-body">
                  <h5 class="mt-0">Commenter Name</h5>
                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                </div>
              </div>

              <div class="media mt-4">
                <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                <div class="media-body">
                  <h5 class="mt-0">Commenter Name</h5>
                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                </div>
              </div> -->

            <!-- </div>
          </div>
 -->
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card mb-4">
            <h5 class="card-header">Pencarian</h5>
            <div class="card-body">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Cari</button>
                </span>
              </div>
            </div>
          </div>

          <!-- Categories Widget -->
          <!-- <div class="card my-4">
            <h5 class="card-header">Categories</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">Web Design</a>
                    </li>
                    <li>
                      <a href="#">HTML</a>
                    </li>
                    <li>
                      <a href="#">Freebies</a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">JavaScript</a>
                    </li>
                    <li>
                      <a href="#">CSS</a>
                    </li>
                    <li>
                      <a href="#">Tutorials</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div> -->

          

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->