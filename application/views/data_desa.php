<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
   <body>
    
    <?php include 'static/navbar.php'; ?>
    <!-- Navigation -->

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Data Desa
        <small>Desa Wukirsari
        </small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Data Desa</li>
      </ol>

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

          <!-- Preview Image -->
          <!-- <img class="img-fluid rounded" src="http://placehold.it/900x300" alt=""> -->

          <hr>

          <p>Desa Wukirsari memiliki ciri khas yang tidak dapat ditemukan oleh desa lain. Hal ini dapat dilihat dari bagaimana wilayah digunakan dan seperti apa penduduk yang tinggal di dalamnya. Untuk mengetahui hal-hal tersebut dapat dilakukan dengan melihat data secara langsung. </p>

          <p>Data Kependudukan Desa Wukirsari : </p>
<ul>
  <li><a href="#">    Data Jumlah Penduduk </a></li>
  <li><a href="#">    Data Jumlah Kepala Keluarga </a></li>
  <li><a href="#">    Data Penduduk Menurut Agama </a></li>
  <li><a href="#">    Data Penduduk Menurut Status Perkawinan </a></li>
  <li><a href="#">    Data Penduduk Menurut Pendidikan </a></li>
  <li><a href="#">    Data Penduduk Menurut Usia Pendidikan </a></li>
  <li><a href="#">    Data Penduduk Menurut Pekerjaan </a></li>
  <li><a href="#">    Data Penduduk Menurut Golongan Darah </a></li>
</ul>

          <hr>

          

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card mb-4">
            <h5 class="card-header">Pencarian</h5>
            <div class="card-body">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Cari</button>
                </span>
              </div>
            </div>
          </div>

          <!-- Categories Widget -->
          <!-- <div class="card my-4">
            <h5 class="card-header">Categories</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">Web Design</a>
                    </li>
                    <li>
                      <a href="#">HTML</a>
                    </li>
                    <li>
                      <a href="#">Freebies</a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">JavaScript</a>
                    </li>
                    <li>
                      <a href="#">CSS</a>
                    </li>
                    <li>
                      <a href="#">Tutorials</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div> -->

          

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
