<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
   <body>
    
    <?php include 'static/navbar.php'; ?>
    <!-- Navigation -->

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Daftar Produk Lokal
        <small>Desa Wukirsari</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Produk</li>
      </ol>

      <div class="row">
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/prod1.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Empek-Empek Lele</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
               <div class="card-footer"><center>
              <a style="cursor: pointer; color: white;" class="btn btn-primary">Rp 15.000/pcs</a>
            </center>
            </div>
          </div>
            </div>
          </div>


        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/prod2.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Keripik Lele</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos quisquam, error quod sed cumque, odio distinctio velit nostrum temporibus necessitatibus et facere atque iure perspiciatis mollitia recusandae vero vel quam!</p>
              <div class="card-footer"><center>
              <a style="cursor: pointer; color: white;" class="btn btn-primary">Rp 15.000/pcs</a>
            </center>
            </div>
          </div>
            </div>
          </div>
        
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/prod3.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Abon Lele</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos quisquam, error quod sed cumque, odio distinctio velit nostrum temporibus necessitatibus et facere atque iure perspiciatis mollitia recusandae vero vel quam!</p>
              <div class="card-footer"><center>
              <a style="cursor: pointer; color: white;" class="btn btn-primary">Rp 15.000/pcs</a>
            </center>
            </div>
          </div>
            </div>
          </div>
        </div>
        
      </div>

      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    <!-- /.container -->