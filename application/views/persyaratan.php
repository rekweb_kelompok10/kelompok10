<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
   <body>
    
    <?php include 'static/navbar.php'; ?>

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Persyaratan Permohonan
        <small>Desa Wukirsari
        </small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Persyaratan Permohonan</li>
      </ol>

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

          <!-- Preview Image -->
          <!-- <img class="img-fluid rounded" src="http://placehold.it/900x300" alt=""> -->

          <hr>

          <p>Berikut ini adalah persyaratan yang harus dibawaoleh pemohon ketika membutuhkan pelayanan dari bagian Pelayanan Umum sesuai keperluan dari pemohon :</p>

<h5> Persyaratan Permohonan Surat Keterangan Domisili </h5>

<ol>
<li>Surat Pengantar Dukuh yang sudah diisi lengkap dan ditanda tangani</li>
<li>Fotocopy Kartu Keluarga daerah asal 1 lembar</li>
<li>Fotocopy KTP daerah asal 1 lembar</li>
<li>Fotocopy Kartu Keluarga yang ditumpangi</li>
<li>Form F1.12 yang sudah diisi lengkap dan ditanda tangani pemohon dan Dukuh</li>
<li>Surat Permohonan Tinggal Sementara yang sudah diisi lengkap dan ditanda tangani</li>
  </ol>

  <h5> Persyaratan Permohonan Legalisir </h5>

<ol>
<li>Surat Pengantar Dukuh yang sudah diisi lengkap dan ditanda tangani</li>
<li>Dokumen Asli</li>
<li>Fotocopy Kartu Keluarga 1 lembar</li>
<li>Fotocopy KTP 1 lembar</li>
  </ol>

  <h5> Persyaratan Permohonan Kartu Tanda Penduduk </h5>

<ol>
<li>Surat Pengantar Dukuh yang sudah diisi lengkap dan ditanda tangani</li>
<li>Form F1.07 yang sudah diisi lengkap dan ditanda tangani Dukuh</li>
<li>Fotocopy Kartu keluarga sebanyak 1 lembar</li>
<li>Dokumen Pendukung berupa Buku Nikah/ Akta Kelahiran/ Ijazah dan atau Dokumen lain</li>
  </ol>


          <hr>

          <!-- Comments Form -->
          <div class="card my-4">
            <h5 class="card-header">Tinggalkan Komentar :</h5>
            <div class="card-body">
              <form>
                <div class="form-group">
                  <textarea class="form-control" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
              </form>
            </div>
          </div>

         

           

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card mb-4">
            <h5 class="card-header">Pencarian</h5>
            <div class="card-body">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Cari</button>
                </span>
              </div>
            </div>
          </div>

          <!-- Categories Widget -->
          <!-- <div class="card my-4">
            <h5 class="card-header">Categories</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">Web Design</a>
                    </li>
                    <li>
                      <a href="#">HTML</a>
                    </li>
                    <li>
                      <a href="#">Freebies</a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">JavaScript</a>
                    </li>
                    <li>
                      <a href="#">CSS</a>
                    </li>
                    <li>
                      <a href="#">Tutorials</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div> -->

          

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
