  <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
  <body>

    <?php include 'static/navbar.php'; ?>

    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('<?php echo base_url(); ?>img/h1.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h2>DESA WUKIRSARI KIYARAN</h2>
              <h5>"Memayu hayuning bawana, memayu hayuning sasama, hambeg adil paramarta"</h5>
              <br>
              <br>
              <br>
              <br>
              <br>
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>img/h2.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h2>VISI DESA WUKIRSARI KIYARAN</h2>
              <h5>Rencana Pembangunan Jangka Menengah Desa Tahun 2015-2020 menetapkan visi yang merupakan cita-cita yang ingin dicapai, yaitu : Wukirsari membangun Masyarakat yang Berkualitas Maju, Mandiri, Sejahtera, Berkeadilan  dilandasi sifat Agamis  berbasis pada usaha Pertanian, Perikanan, Peternakan untuk menuju  desa yang ber Martabat.</h5>
              <br>
              <br>
              <br>
              <br>
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('<?php echo base_url(); ?>img/h3.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h2>MISI DESA WUKIRSARI KIYARAN</h2>
              <h5>Misi Desa Wukirsari merupakan penjabaran yang lebih operasional dari visi. dengan mempertimbangkan potensi dan permasalahan yang ada di Desa Wukirsari.</h5>
              <br>
              <br>
              <br>
              <br>
              <br>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- Page Content -->
    <div class="container">

      <h1 class="my-4">Selamat Datang di Situs Desa Wukirsari Kiyaran</h1>

  <!-- Features Section -->
      <div class="row">
        <div class="col-lg-6">
          <br>
          <h2>Desa Wukirsari Kiyaran</h2>

          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus undeLorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde</p>
        </div>
        <div class="col-lg-6">
          <div class="agenda">
        <h1 class="my-4">Agenda Desa</h1>
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Waktu</th>
                        <th>Acara</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <td class="agenda-date" class="active" rowspan="1">
                            <div class="dayofmonth">8</div>
                            <div class="dayofweek">Jumat</div>
                            <div class="shortdate text-muted">Desember, 2017</div>
                        </td>
                        <td class="agenda-time">
                            19.30 WIB
                        </td>
                        <td class="agenda-events">
                            <div class="agenda-event">
                                <i class="glyphicon glyphicon-repeat text-muted" title="Repeating event"></i> 
                                Pengajian Rutin
                            </div>
                        </td>
                    </tr>
                    
                    <!-- Multiple events in a single day (note the rowspan) -->
                    <tr>
                        <td class="agenda-date" class="active" rowspan="3">
                            <div class="dayofmonth">10</div>
                            <div class="dayofweek">Minggu</div>
                            <div class="shortdate text-muted">Desember, 2017</div>
                        </td>
                        <td class="agenda-time">
                            8:00 - 9:00 WIB
                        </td>
                        <td class="agenda-events">
                            <div class="agenda-event">
                                Kerja Bakti di Dusun ...
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="agenda-time">
                            10:15 - 11.00 WIB
                        </td>
                        <td class="agenda-events">
                            <div class="agenda-event">
                                Istirahat
                            </div>
                        </td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
        </div>
      </div>
      <!-- /.row -->

      <hr>

<!-- Marketing Icons Section -->
<h2>Produk Desa Wukirsari Kiyaran</h2>
      <div class="row">
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Empek-Empek Lele</h4>
            <div class="card-body">
              <a href="#"><img class="card-img-top" src="<?php echo base_url(); ?>img/produk1.jpg" alt=""></a>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.</p>
            </div>
            <div class="card-footer"><center>
              <a style="cursor: pointer; color: white;" class="btn btn-primary">Rp 15.000/pcs</a>
            </center>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Keripik Lele</h4>
            <div class="card-body">
              <a href="#"><img class="card-img-top" src="<?php echo base_url(); ?>img/produk2.jpg" alt=""></a>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ipsam eos, nam perspiciatis natus commodi similique totam consectetur praesentium molestiae atque exercitationem ut consequuntur, sed eveniet, magni nostrum sint fuga.</p>
            </div>
            <div class="card-footer"><center>
              <a style="cursor: pointer; color: white;" class="btn btn-primary">Rp 12.000/pcs</a>
            </center>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Abon Lele</h4>
            <div class="card-body">
              <a href="#"><img class="card-img-top" src="<?php echo base_url(); ?>img/produk3.jpg" alt=""></a>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.</p>
            </div>
            <div class="card-footer"><center>
              <a style="cursor: pointer; color: white;" class="btn btn-primary">Rp 20.000/pcs</a>
            </center>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->

      <hr>

      <!-- Call to Action Section -->
      <div class="row mb-4">
        <div class="col-md-8">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
        </div>
        <div class="col-md-4">
          <a class="btn btn-lg btn-secondary btn-block" style="font-weight: bold; color: white; cursor: pointer;">Pesan (+62 821-3652-6453)</a>
        </div>
      </div>
<hr>

 <!-- Galleries Section -->
      <div class="row">
        <div class="col-lg-6">
          <img class="img-fluid rounded" src="img/desa.jpg" alt="">
        </div>
        <div class="col-lg-6">
          <img class="img-fluid rounded" src="img/desa.jpg" alt="">
        
        </div>
        </div>
      <!-- /.row -->
<hr>

    </div>
    <!-- /.container -->
<br>

