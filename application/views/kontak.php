<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
   <body>
    
    <?php include 'static/navbar.php'; ?>
    <!-- Navigation -->

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Kontak Kami
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Kontak</li>
      </ol>

      <!-- Content Row -->
      <div class="row">
        <!-- Map Column -->
        <div class="col-lg-8 mb-4">
          <!-- Embedded Google Map -->
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3954.2358302714997!2d110.44161141386968!3d-7.657773777893989!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a5dc68918b915%3A0x567acbbb0a57d3cc!2sWukirsari+Village+Government+Office!5e0!3m2!1sen!2sid!4v1513331601429" frameborder="0" width="100%" height="400px" scrolling="no" marginheight="0" marginwidth="0" allowfullscreen></iframe>
        </div>
        <!-- Contact Details Column -->
        <div class="col-lg-4 mb-4">
          <h3>Informasi Kontak</h3>
          <p>
            Komplek Balai Desa Wukirsari-Kiyaran 
            <br>Wukirsari, Kecamatan Cangkringan, Sleman
            <br>
          </p>
          <p>
            <abbr title="Nomor Telepon">P</abbr>: (0274) 897 114
          </p>
          <p>
            <abbr title="Email">E</abbr>:
            <a href="mailto:name@example.com">wukirsariku.kiyaran@gmail.com
            </a>
          </p>
          <p>
            <abbr title="Waktu Pelayanan">H</abbr>: Senin - Jumat (Pukul 09:00 - 15:00 WIB)
          </p>
        </div>
      </div>
      <!-- /.row -->

      <!-- Contact Form -->
      <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
      <div class="row">
        <div class="col-lg-8 mb-4">
          <h3>Kirim Pesan</h3>
          <form name="sentMessage" id="contactForm" novalidate>
            <div class="control-group form-group">
              <div class="controls">
                <label>Nama:</label>
                <input type="text" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                <p class="help-block"></p>
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label>Nomor Ponsel:</label>
                <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number.">
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label>Alamat Email:</label>
                <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address.">
              </div>
            </div>
            <div class="control-group form-group">
              <div class="controls">
                <label>Pesan:</label>
                <textarea rows="10" cols="100" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
              </div>
            </div>
            <div id="success"></div>
            <!-- For success/fail messages -->
            <button type="submit" class="btn btn-primary" id="sendMessageButton">Kirim</button>
          </form>
        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
