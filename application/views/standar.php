<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
   <body>
    
    <?php include 'static/navbar.php'; ?>

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Standar Pelayanan
        <small>Desa Wukirsari
        </small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Standar Pelayanan</li>
      </ol>

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

          <!-- Preview Image -->
          <!-- <img class="img-fluid rounded" src="http://placehold.it/900x300" alt=""> -->

          <hr>

          <p>Bagian Pelayanan Umum di Kantor Kelurahan Wukirsari merupakan tempat dilakukannya pelayanan bagi masyarakat yang hendak mengurus surat-surat kependudukan. Pelayanan tersebut tentunya mengikuti sebuah standar yang telah ditetapkan. </p>

          <p>Berikut ini standar pelayanan yang digunakan bagi tiap-tiap surat yang diinginkan oleh pemohon : </p>
<ol>
  <li><a href="#">    Standar Pelayanan Permohonan Surat Keterangan Domisili </a></li>
  <li><a href="#">    Standar Pelayanan Permohonan Legalisir </a></li>
  <li><a href="#">    Standar Pelayanan Permohonan  Surat Keterangan </a></li>
  <li><a href="#">    Standar Pelayanan Permohonan Kartu Tanda Penduduk (KTP) </a></li>
  <li><a href="#">    Standar Pelayanan Permohonan Kartu Keluarga </a></li>
  <li><a href="#">    Standar Pelayanan Permohonan Surat Keterangan Lahir </a></li>
  <li><a href="#">    Standar Pelayanan Permohonan Surat Keterangan Kematian </a></li>
  <li><a href="#">    Standar Pelayanan Permohonan Penduduk Datang </a></li>
  <li><a href="#">    Standar Pelayanan Permohonan Pindah Penduduk </a></li>
  <li><a href="#">    Standar Pelayanan Permohonan Surat Keterangan Tinggal Sementara </a></li>
</ol>

          <hr>

          

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card mb-4">
            <h5 class="card-header">Pencarian</h5>
            <div class="card-body">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Cari</button>
                </span>
              </div>
            </div>
          </div>

          <!-- Categories Widget -->
          <!-- <div class="card my-4">
            <h5 class="card-header">Categories</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">Web Design</a>
                    </li>
                    <li>
                      <a href="#">HTML</a>
                    </li>
                    <li>
                      <a href="#">Freebies</a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">JavaScript</a>
                    </li>
                    <li>
                      <a href="#">CSS</a>
                    </li>
                    <li>
                      <a href="#">Tutorials</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div> -->

          

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
