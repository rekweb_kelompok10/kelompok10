
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url(); ?>admin">Dashboard</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-institution"></i> Struktur Pemerintahan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url(); ?>admin/profil">Profil</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/organisasi">Struktur Organisasi</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/visimisi">Visi dan Misi</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-institution"></i> Pelayanan Umum <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url(); ?>admin/standar">Standar Pelayanan</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/persyaratan">Persyaratan Permohonan</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-file-text-o"></i> Data Desa <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url(); ?>admin/jumlah_penduduk">Data Jumlah Penduduk</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/jumlah_kk">Data Jumlah KK</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/agama">Data Agama</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/status_perkawinan">Data Status Perkawinan</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/data_pendidikan">Data Pendidikan</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/data_pekerjaan">Data Pekerjaan</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/golongan_darah">Data Golongan Darah</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/daftar_produk">Data Produk</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/data_agenda">Data Agenda</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-newspaper-o"></i> Artikel <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url(); ?>admin/list_artikel">List Artikel</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/tambah_artikel">Tambah Artikel</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-picture-o"></i> Galeri <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url(); ?>admin/list_galeri">List Galeri</a></li>
                      <li><a href="<?php echo site_url(); ?>admin/tambah_gambar">Tambah Gambar</a></li>
                    </ul>
                  </li>
                </ul>
              </div>

            </div>