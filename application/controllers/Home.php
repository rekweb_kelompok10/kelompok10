<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{
    public function index() {
        $this->load->view('static/header');
        $this->load->view('home');
        $this->load->view('static/footer');
    }
    public function persyaratan() {
        $this->load->view('static/header'); 
        $this->load->model('Post_model');
        $data = array(
                'record' => $this->Post_model->read('persyaratan', 'desa_persyaratan')
            );

        $this->load->view('persyaratan', $data);
        $this->load->view('static/footer');
    }

    public function artikel() {
        $this->load->view('static/header');
        $this->load->view('artikel');
        $this->load->view('static/footer');
    }
    public function produk() {
        $this->load->view('static/header');
        $this->load->view('produk');
        $this->load->view('static/footer');
    }
    public function agenda() {
        $this->load->view('static/header');
        $this->load->model('Post_model');
        $data = array(
                'record' => $this->Post_model->read('desa_agenda',NULL,NULL)
            );
        $this->load->view('agenda',$data);
        $this->load->view('static/footer');
    }
    public function galeri() {
        $this->load->view('static/header');
        $this->load->view('galeri');
        $this->load->view('static/footer');
    }
    public function kontak() {
        $this->load->view('static/header');
        $this->load->view('kontak');
        $this->load->view('static/footer');
    }
}
