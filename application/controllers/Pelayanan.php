<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan extends CI_Controller{
    
    public function persyaratan() {
        $this->load->view('static/header');
        $this->load->view('persyaratan');
        $this->load->view('static/footer');
    }
    
    public function standar() {
        $this->load->view('static/header');
        $this->load->view('standar');
        $this->load->view('static/footer');
    }
}
