<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller{
    public function index() {      
        $this->load->view('admin/index');
    }

    public function profil() {
        $this->load->helper('form');
		$this->load->library('form_validation');		
		$this->load->model('Post_model');
        $data = array(
					'record' => $this->Post_model->edit('profil', 'desa_struktur')
				);

			$this->load->view('admin/profil_desa', $data);
    }
    
    function update_struktur() {
        $post = $this->input->post();
        $this->load->model('Post_model');
        $this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('judul', 'judul', 'required');
		$this->form_validation->set_rules('isi', 'isi', 'required');
		$this->form_validation->set_message('required','alert("%s masih kosong, silahkan diisi");');
		$this->form_validation->set_error_delimiters('<script>', '</script>');

		if($this->form_validation->run() == TRUE){
			$this->load->model('Post_model');
        	$data = array(
						'judul' => $post['judul'],
						'isi' => $post['isi'],
						'updated_at' => date('Y-m-d')
					);

				$this->Post_model->update($post['role'],$data,'desa_struktur');
				redirect(base_url('admin/'.$post['role']));			
			} else {
                $data = array(
					'record' => $this->Post_model->edit($post['role'], 'desa_struktur')
				);

			    $this->load->view('admin/'.$post['slug'], $data);	
            }
		}

        function update_persyaratan() {
        $post = $this->input->post();
        $this->load->model('Post_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('judul', 'judul', 'required');
        $this->form_validation->set_rules('isi', 'isi', 'required');
        $this->form_validation->set_message('required','alert("%s masih kosong, silahkan diisi");');
        $this->form_validation->set_error_delimiters('<script>', '</script>');

        if($this->form_validation->run() == TRUE){
            $this->load->model('Post_model');
            $data = array(
                        'judul' => $post['judul'],
                        'isi' => $post['isi'],
                        'updated_at' => date('Y-m-d')
                    );

                $this->Post_model->update($post['role'],$data,'desa_persyaratan');
                redirect(base_url('admin/'.$post['role']));         
            } else {
                $data = array(
                    'record' => $this->Post_model->edit($post['role'], 'desa_persyaratan')
                );

                $this->load->view('admin/'.$post['slug'], $data);   
            }
        }
    
        function update_pelayanan() {
        $post = $this->input->post();
        $this->load->model('Post_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('isi', 'Isi', 'required');
        $this->form_validation->set_message('required','alert("%s masih kosong, silahkan diisi"); history.go(-1);');
        $this->form_validation->set_error_delimiters('<script>', '</script>');

        if($this->form_validation->run() == TRUE){
            $this->load->model('Post_model');
            $data = array(
                        'judul' => $post['judul'],
                        'isi' => $post['isi'],
                        'updated_at' => date('Y-m-d')
                    );

                $this->Post_model->update($post['role'],$data,'desa_pelayanan');
                redirect(base_url('admin/'.$post['role']));         
            } else {
                echo validation_errors();
            }
        }
    
    function update_status() {
        $post = $this->input->post();
        $this->load->model('Post_model');
        $this->load->helper(array('form', 'url'));
            $data = array(
                        'belum_kawin' => $post['belum_kawin'],
                        'kawin' => $post['kawin'],
                        'cerai_hidup' => $post['cerai_hidup'],
                        'cerai_mati' => $post['cerai_mati']
                    );

                $this->Post_model->update_by_id($post['id'],$data,'desa_status_kawin');
                redirect(base_url('admin/status_perkawinan'));         
        }
    
     function update_agama() {
        $post = $this->input->post();
        $this->load->model('Post_model');
        $this->load->helper(array('form', 'url'));
            $data = array(
                        'islam' => $post['islam'],
                        'kristen' => $post['kristen'],
                        'katholik' => $post['katholik'],
                        'hindu' => $post['hindu'],
                        'buddha' => $post['buddha']
                    );

                $this->Post_model->update_by_id($post['id'],$data,'desa_agama');
                redirect(base_url('admin/agama'));         
        }
    
         function update_pekerjaan() {
        $post = $this->input->post();
        $this->load->model('Post_model');
        $this->load->helper(array('form', 'url'));
            $data = array(
                        'pekerjaan' => $post['pekerjaan'],
                        'jml_laki' => $post['jml_laki'],
                        'jml_perempuan' => $post['jml_perempuan']
                    );

                $this->Post_model->update_by_id($post['id'],$data,'desa_pekerjaan');
                redirect(base_url('admin/data_pekerjaan'));         
        }
    
    function update_goldar() {
        $post = $this->input->post();
        $this->load->model('Post_model');
        $this->load->helper(array('form', 'url'));
            $data = array(
                    'goldar_o' => $post['goldar_o'],
                    'goldar_a' => $post['goldar_a'],
                    'goldar_b' => $post['goldar_b'],
                    'goldar_ab' => $post['goldar_ab']
                    );

                $this->Post_model->update_by_id($post['id'],$data,'desa_golongan_darah');
                redirect(base_url('admin/golongan_darah'));         
        }
    
    
        function update_pendidikan() {
        $post = $this->input->post();
        $this->load->model('Post_model');
        $this->load->helper(array('form', 'url'));
            $data = array(
                        'belum_sekolah' => $post['belum_sekolah'],
                        'belum_tamat_sd' => $post['belum_tamat_sd'],
                        'tamat_sd' => $post['tamat_sd'],
                        'smp' => $post['smp'],
                        'sma' => $post['sma'],
                        'd1' => $post['d1'],
                        'd3' => $post['d3'],
                        's1' => $post['s1'],
                        's2' => $post['s2']
                    );

                $this->Post_model->update_by_id(1,$data,'desa_pendidikan');
                redirect(base_url('admin/data_pendidikan'));         
        }
    
    
    function update_penduduk() {
        $post = $this->input->post();
        $this->load->model('Post_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('dusun', 'dusun', 'required');
        $this->form_validation->set_rules('jml_laki', 'Jumlah LAki-laki', 'required');
        $this->form_validation->set_rules('jml_perempuan', 'Jumlah LAki-perempuan', 'required');
        $this->form_validation->set_message('required','alert("%s masih kosong, silahkan diisi"); history.go(-1);');
        $this->form_validation->set_error_delimiters('<script>', '</script>');

        if($this->form_validation->run() == TRUE){
            $this->load->model('Post_model');
            $data = array(
                        'dusun' => $post['dusun'],
                        'jml_laki' => $post['jml_laki'],
                        'jml_perempuan' => $post['jml_perempuan']
                    );

                $this->Post_model->update_by_id($post['id'],$data,'desa_jml_penduduk');
                redirect(base_url('admin/jumlah_penduduk'));         
            } else {
                echo validation_errors();
            }
        }

            function update_produk() {
        $this->load->library('form_validation');
        $post = $this->input->post();           
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = 300;
                $config['max_width'] = 1024;
                $config['max_height'] = 768;

                $this->load->library('upload', $config);
                if($this->upload->do_upload('gambar')){
                    $upload_data = $this->upload->data();
                    $path = 'uploads/'.$upload_data['file_name'];
                    
                    $this->load->model('Post_model');
            $data = array(
                    'nama' => $post['nama'],
                    'harga' => $post['harga'],
                    'deskripsi' => $post['deskripsi'],
                    'kontak' => $post['kontak'],
                    'gambar' => $path
                );

                $this->Post_model->update_by_id($post['id'],$data,'desa_produk');
                redirect(base_url('admin/daftar_produk'));         
        }else{
                    echo $this->upload->display_errors();
                }
    }

        function update_agenda() {
        $post = $this->input->post();
        $this->load->model('Post_model');
        $this->load->helper(array('form', 'url'));
            $data = array(
                    'tanggal_acara' => $post['tanggal_acara'],
                    'waktu_acara' => $post['waktu_acara'],
                    'kegiatan_acara' => $post['kegiatan_acara'],
                    'lokasi_acara' => $post['lokasi_acara'],
                    );

                $this->Post_model->update_by_id($post['id'],$data,'desa_agenda');
                redirect(base_url('admin/data_agenda'));         
        }


        public function organisasi() {
        $this->load->helper('form');
        $this->load->library('form_validation');        
        $this->load->model('Post_model');
        $data = array(
                    'record' => $this->Post_model->edit('organisasi', 'desa_struktur')
                );
            $this->load->view('admin/struktur_organisasi', $data);
            
    }

    public function visimisi() {
       $this->load->helper('form');
        $this->load->library('form_validation');        
        $this->load->model('Post_model');
        $data = array(
                    'record' => $this->Post_model->edit('visimisi', 'desa_struktur')
                );
            $this->load->view('admin/visi_misi', $data);
            
    }

    public function standar() {
        $this->load->model('Post_model');

		$data = array(
				'record' => $this->Post_model->read('desa_pelayanan',NULL,NULL)
			);

		$this->load->view('admin/standar_pelayanan', $data);
    }
    
    public function edit_pelayanan($id=0){
		$this->load->helper('form');	
		$this->load->model('Post_model');

		if($id != 0 && !empty($id)){
			$data = array(
					'record' => $this->Post_model->edit_by_id('standar', $id , 'desa_pelayanan')
				);

			$this->load->view('admin/edit_pelayanan', $data);
				
		}
		else{
			redirect(base_url('admin/standar_pelayanan'));
		}
	}
    
    public function edit_goldar($id=0){
		$this->load->helper('form');	
		$this->load->model('Post_model');

		if($id != 0 && !empty($id)){
			$data = array(
					'record' => $this->Post_model->edit_by_id(NULL, $id , 'desa_golongan_darah')
				);

			$this->load->view('admin/edit_goldar', $data);
				
		}
		else{
			redirect(base_url('admin/golongan_darah'));
		}
	}

    public function edit_produk($id=0){
        $this->load->helper('form');    
        $this->load->model('Post_model');

        if($id != 0 && !empty($id)){
            $data = array(
                    'record' => $this->Post_model->edit_by_id(NULL, $id , 'desa_produk')
                );

            $this->load->view('admin/edit_produk', $data);
                
        }
        else{
            redirect(base_url('admin/daftar_produk'));
        }
    }

    public function edit_agenda($id=0){
        $this->load->helper('form');    
        $this->load->model('Post_model');

        if($id != 0 && !empty($id)){
            $data = array(
                    'record' => $this->Post_model->edit_by_id(NULL, $id , 'desa_agenda')
                );

            $this->load->view('admin/edit_agenda', $data);
                
        }
        else{
            redirect(base_url('admin/data_agenda'));
        }
    }


    
    public function tambah_pelayanan(){
			$post = $this->input->post();	
            $this->load->model('Post_model');
			$data = array(
					'judul' => $post['judul'],
					'isi' => $post['isi'],
                    'role' => 'standar',
					'created_at' => date('Y-m-d')
				);
				$this->Post_model->create('desa_pelayanan',$data);
				redirect(base_url('admin/standar'));
      }
    
    public function add_artikel(){
			$post = $this->input->post();	
            $this->load->model('Post_model');
            if($post['Public']) {
                $public = $post['Public'];
            } else {
                $public = 'Private';
            }
			$data = array(
					'judul_artikel' => $post['judul'],
					'isi_artikel' => $post['descr'],
                    'author' => $post['author'],
                    'akses' => $public,
					'created_at' => date('Y-m-d')
				);
				$this->Post_model->create('desa_artikel',$data);
				redirect(base_url('admin/list_artikel'));
      } 		
	
    public function delete_standar($id=0){
            $this->load->model('Post_model');
            $this->Post_model->delete($id,'desa_pelayanan');
            redirect(base_url('admin/standar'));
    }
    

    
    public function persyaratan() {    
        $this->load->model('Post_model');

        $data = array(
                'record' => $this->Post_model->edit('persyaratan', 'desa_persyaratan')
            );

        $this->load->view('admin/persyaratan_permohonan', $data);
    }

    public function jumlah_penduduk() {    
        $this->load->model('Post_model');

		$data = array(
				'record' => $this->Post_model->read('desa_jml_penduduk',NULL,NULL)
			);
        $this->load->view('admin/data_jumlah_penduduk', $data);
        }
    
     public function edit_penduduk($id=0){
		$this->load->helper('form');	
		$this->load->model('Post_model');

		if($id != 0 && !empty($id)){
			$data = array(
					'record' => $this->Post_model->edit_by_id(NULL, $id , 'desa_jml_penduduk')
				);

			$this->load->view('admin/edit_penduduk', $data);
				
		}
		else{
			redirect(base_url('admin/data_jumlah_penduduk'));
		}
	}
    
    public function edit_pendidikan($id=0){
		$this->load->helper('form');	
		$this->load->model('Post_model');

		if($id != 0 && !empty($id)){
			$data = array(
					'record' => $this->Post_model->edit_by_id(NULL, $id , 'desa_pendidikan')
				);

			$this->load->view('admin/edit_pendidikan', $data);
				
		}
		else{
			redirect(base_url('admin/data_pendidikan'));
		}
	}
    
        public function edit_pekerjaan($id=0){
		$this->load->helper('form');	
		$this->load->model('Post_model');

		if($id != 0 && !empty($id)){
			$data = array(
					'record' => $this->Post_model->edit_by_id(NULL, $id , 'desa_pekerjaan')
				);

			$this->load->view('admin/edit_pekerjaan', $data);
				
		}
		else{
			redirect(base_url('admin/data_pekerjaan'));
		}
	}
    
    public function agama() {    
       $this->load->model('Post_model');

        $data = array(
                'record' => $this->Post_model->read('desa_agama',NULL,NULL)
            );
        $this->load->view('admin/data_agama', $data);
        }

        public function tambah_agama(){
            $post = $this->input->post();   
            $this->load->model('Post_model');
            $data = array(
                    'dusun' => $post['dusun'],
                    'islam' => $post['islam'],
                    'katholik' => $post['katholik'],
                    'kristen' => $post['kristen'],
                    'hindu' => $post['hindu'],
                    'budha' => $post['budha'],
                );
                $this->Post_model->create('desa_agama',$data);
                redirect(base_url('admin/agama'));
      } 

      public function delete_agama($id=0){
            $this->load->model('Post_model');
            $this->Post_model->delete($id,'desa_agama');
            redirect(base_url('admin/agama'));
    }
    
    public function jumlah_kk() {
        $this->load->view('admin/data_jumlah_kk');
    }

    public function status_perkawinan() {
        $this->load->model('Post_model');

		$data = array(
				'record' => $this->Post_model->read('desa_status_kawin',NULL,NULL)
			);

		$this->load->view('admin/status_perkawinan', $data);
    }
    
    public function edit_status($id=0){
		$this->load->helper('form');	
		$this->load->model('Post_model');

		if($id != 0 && !empty($id)){
			$data = array(
					'record' => $this->Post_model->edit_by_id(NULL, $id , 'desa_status_kawin')
				);

			$this->load->view('admin/edit_status_kawin', $data);
				
		}
		else{
			redirect(base_url('admin/status_perkawinan'));
		}
	}
    
    public function edit_agama($id=0){
		$this->load->helper('form');	
		$this->load->model('Post_model');

		if($id != 0 && !empty($id)){
			$data = array(
					'record' => $this->Post_model->edit_by_id(NULL, $id , 'desa_agama')
				);

			$this->load->view('admin/edit_agama', $data);
				
		}
		else{
			redirect(base_url('admin/agama'));
		}
	}
    
    public function data_pendidikan() {    
        $this->load->model('Post_model');

        $data = array(
                'record' => $this->Post_model->read('desa_pendidikan',NULL,NULL)
            );
        $this->load->view('admin/data_pendidikan', $data);
        }

         public function tambah_pendidikan(){
            $post = $this->input->post();   
            $this->load->model('Post_model');
            $data = array(
                    'belum_sekolah' => $post['belum_sekolah'],
                    'belum_tamat_sd' => $post['belum_tamat_sd'],
                    'tamat_sd' => $post['tamat_sd'],
                    'smp' => $post['smp'],
                    'sma' => $post['sma'],
                    'd1' => $post['d1'],
                    'd3' => $post['d3'],
                    's1' => $post['s1'],
                    's2' => $post['s2'],
                );
                $this->Post_model->create('desa_pendidikan',$data);
                redirect(base_url('admin/data_pendidikan'));
      } 

      public function delete_pendidikan($id=0){
            $this->load->model('Post_model');
            $this->Post_model->delete($id,'desa_pendidikan');
            redirect(base_url('admin/data_pendidikan'));
    }

    public function data_pekerjaan() { 
        $this->load->model('Post_model');

        $data = array(
                'record' => $this->Post_model->read('desa_pekerjaan',NULL,NULL)
            );
        $this->load->view('admin/data_pekerjaan', $data);
        }

         public function tambah_pekerjaan(){
            $post = $this->input->post();   
            $this->load->model('Post_model');
            $data = array(
                    'pekerjaan' => $post['pekerjaan'],
                    'jml_laki' => $post['jml_laki'],
                    'jml_perempuan' => $post['jml_perempuan'],
                );
                $this->Post_model->create('desa_pekerjaan',$data);
                redirect(base_url('admin/data_pekerjaan'));
      } 

      public function delete_pekerjaan($id=0){
            $this->load->model('Post_model');
            $this->Post_model->delete($id,'desa_pekerjaan');
            redirect(base_url('admin/data_pekerjaan'));
    }

   public function golongan_darah() {   
        $this->load->model('Post_model');

        $data = array(
                'record' => $this->Post_model->read('desa_golongan_darah',NULL,NULL)
            );
        $this->load->view('admin/golongan_darah', $data);
        }

         public function tambah_goldar(){
            $post = $this->input->post();   
            $this->load->model('Post_model');
            $data = array(
                    'jenis_kelamin' => $post['jenis_kelamin'],
                    'goldar_o' => $post['goldar_o'],
                    'goldar_a' => $post['goldar_a'],
                    'goldar_b' => $post['goldar_b'],
                    'goldar_ab' => $post['goldar_ab'],
                );
                $this->Post_model->create('desa_golongan_darah',$data);
                redirect(base_url('admin/golongan_darah'));
      } 

      public function delete_goldar($id=0){
            $this->load->model('Post_model');
            $this->Post_model->delete($id,'desa_golongan_darah');
            redirect(base_url('admin/golongan_darah'));
    }


    public function list_artikel() {   
        $this->load->model('Post_model');

		$data = array(
				'record' => $this->Post_model->read('desa_artikel', 'NULL', 'NULL')
			);
        $this->load->view('admin/list_artikel',$data);
    }

    public function tambah_artikel() {   
        $this->load->view('admin/tambah_artikel');
    }

    public function list_galeri() {    
        $this->load->view('admin/list_galeri');
    }

    public function tambah_gambar() {    
        $this->load->view('admin/tambah_gambar');
    }

    public function daftar_produk() {   
        $this->load->model('Post_model');

        $data = array(
                'record' => $this->Post_model->read('desa_produk',NULL,NULL)
            );
        $this->load->view('admin/daftar_produk', $data);
        }

         public function tambah_produk(){
            $this->load->helper('form');
        $this->load->library('form_validation');
        $post = $this->input->post();           


                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = 300;
                $config['max_width'] = 1024;
                $config['max_height'] = 768;

                $this->load->library('upload', $config);
                if($this->upload->do_upload('gambar')){
                    $upload_data = $this->upload->data();
                    $path = 'uploads/'.$upload_data['file_name'];
                    
                    $this->load->model('Post_model');
            $data = array(
                    'nama' => $post['nama'],
                    'harga' => $post['harga'],
                    'deskripsi' => $post['deskripsi'],
                    'kontak' => $post['kontak'],
                    'gambar' => $path
                );
                $this->Post_model->create('desa_produk',$data);
                redirect(base_url('admin/daftar_produk'));

      }else{
                    echo $this->upload->display_errors();
                }
      } 

      public function delete_produk($id=0){
            $this->load->model('Post_model');
            $this->Post_model->delete($id,'desa_produk');
            redirect(base_url('admin/daftar_produk'));
    }

    public function data_agenda() {   
        $this->load->model('Post_model');

        $data = array(
                'record' => $this->Post_model->read('desa_agenda',NULL,NULL)
            );
        $this->load->view('admin/data_agenda', $data);
        }

         public function tambah_agenda(){
            $post = $this->input->post();   
            $this->load->model('Post_model');
            $data = array(
                    'tanggal_acara' => $post['tanggal_acara'],
                    'waktu_acara' => $post['waktu_acara'],
                    'kegiatan_acara' => $post['kegiatan_acara'],
                    'lokasi_acara' => $post['lokasi_acara'],
                );
                $this->Post_model->create('desa_agenda',$data);
                redirect(base_url('admin/data_agenda'));
      } 

      public function delete_agenda($id=0){
            $this->load->model('Post_model');
            $this->Post_model->delete($id,'desa_agenda');
            redirect(base_url('admin/data_agenda'));
    }
}


