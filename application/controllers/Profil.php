<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller{
    
    public function visimisi() {
        $this->load->view('static/header');
        $this->load->view('visi_misi');
        $this->load->view('static/footer');
    }

    public function struktur() {
        $this->load->view('static/header');
        $this->load->view('struktur');
        $this->load->view('static/footer');
    }

    public function potensi() {
        $this->load->view('static/header');
        $this->load->view('potensi');
        $this->load->view('static/footer');
    }

    public function data_desa() {
        $this->load->view('static/header');
        $this->load->view('data_desa');
        $this->load->view('static/footer');
    }
    
}
