-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 30, 2017 at 10:46 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `desa_wukirsari`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(8) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_agama`
--

CREATE TABLE `desa_agama` (
  `id` int(8) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `islam` int(11) NOT NULL,
  `katholik` int(11) NOT NULL,
  `kristen` int(11) NOT NULL,
  `hindu` int(11) NOT NULL,
  `buddha` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_artikel`
--

CREATE TABLE `desa_artikel` (
  `id` int(8) NOT NULL,
  `judul_artikel` varchar(255) NOT NULL,
  `isi_artikel` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `tanggal` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `desa_galeri`
--

CREATE TABLE `desa_galeri` (
  `id` int(8) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_golongan_darah`
--

CREATE TABLE `desa_golongan_darah` (
  `id` int(8) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `goldar_o` int(11) NOT NULL,
  `goldar_a` int(11) NOT NULL,
  `goldar_b` int(11) NOT NULL,
  `goldar_ab` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_jml_kk`
--

CREATE TABLE `desa_jml_kk` (
  `id` int(8) NOT NULL,
  `dusun` int(11) NOT NULL,
  `jml_laki` int(11) NOT NULL,
  `jml_perempuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_jml_penduduk`
--

CREATE TABLE `desa_jml_penduduk` (
  `id` int(8) NOT NULL,
  `dusun` int(11) NOT NULL,
  `jml_laki` int(11) NOT NULL,
  `jml_perempuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_pekerjaan`
--

CREATE TABLE `desa_pekerjaan` (
  `id` int(8) NOT NULL,
  `pekerjaan` varchar(255) NOT NULL,
  `jml_laki` int(11) NOT NULL,
  `jml_perempuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_pendidikan`
--

CREATE TABLE `desa_pendidikan` (
  `id` int(8) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `belum_sekolah` int(11) NOT NULL,
  `belum_tamat_sd` int(11) NOT NULL,
  `tamat_sd` int(11) NOT NULL,
  `smp` int(11) NOT NULL,
  `sma` int(11) NOT NULL,
  `d1` int(11) NOT NULL,
  `d3` int(11) NOT NULL,
  `s1` int(11) NOT NULL,
  `s2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_produk`
--

CREATE TABLE `desa_produk` (
  `id` int(8) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_status_kawin`
--

CREATE TABLE `desa_status_kawin` (
  `id` int(8) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `belum_kawin` int(11) NOT NULL,
  `kawin` int(11) NOT NULL,
  `cerai_mati` int(11) NOT NULL,
  `cerai_hidup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa_struktur`
--

CREATE TABLE `desa_struktur` (
  `id` int(8) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `role` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_At` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desa_struktur`
--

INSERT INTO `desa_struktur` (`id`, `judul`, `isi`, `role`, `created_at`, `updated_At`) VALUES
(1, 'jnknkjnj', 'sdscsdcscsdcsdcsdcsdcsdcsdcsdcsdcsd', 'profil', '2017-12-30 00:00:00', '2017-12-30 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `desa_usia_penduduk`
--

CREATE TABLE `desa_usia_penduduk` (
  `id` int(8) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `nol_hingga_tiga` int(11) NOT NULL,
  `empat_hingga_enam` int(11) NOT NULL,
  `tujuh_hingga_duabelas` int(11) NOT NULL,
  `tigabelas_hingga_limabelas` int(11) NOT NULL,
  `enambelas_hingga_delapanbelas` int(11) NOT NULL,
  `sembilanbelas_keatas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_agama`
--
ALTER TABLE `desa_agama`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_artikel`
--
ALTER TABLE `desa_artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_galeri`
--
ALTER TABLE `desa_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_golongan_darah`
--
ALTER TABLE `desa_golongan_darah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_jml_kk`
--
ALTER TABLE `desa_jml_kk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_jml_penduduk`
--
ALTER TABLE `desa_jml_penduduk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_pekerjaan`
--
ALTER TABLE `desa_pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_pendidikan`
--
ALTER TABLE `desa_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_produk`
--
ALTER TABLE `desa_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_status_kawin`
--
ALTER TABLE `desa_status_kawin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_struktur`
--
ALTER TABLE `desa_struktur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa_usia_penduduk`
--
ALTER TABLE `desa_usia_penduduk`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_agama`
--
ALTER TABLE `desa_agama`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_artikel`
--
ALTER TABLE `desa_artikel`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_galeri`
--
ALTER TABLE `desa_galeri`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_golongan_darah`
--
ALTER TABLE `desa_golongan_darah`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_jml_kk`
--
ALTER TABLE `desa_jml_kk`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_jml_penduduk`
--
ALTER TABLE `desa_jml_penduduk`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_pekerjaan`
--
ALTER TABLE `desa_pekerjaan`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_pendidikan`
--
ALTER TABLE `desa_pendidikan`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_produk`
--
ALTER TABLE `desa_produk`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_status_kawin`
--
ALTER TABLE `desa_status_kawin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa_struktur`
--
ALTER TABLE `desa_struktur`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `desa_usia_penduduk`
--
ALTER TABLE `desa_usia_penduduk`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */; 
